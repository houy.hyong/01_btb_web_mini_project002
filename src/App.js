import "./App.css";
import LandingPage from "./page/LandingPage";
import Register from "./components/register_login/Register";
import Login from "./components/register_login/Login";
import HomePage from "./page/HomePage";
import PrivateRoutes from "./utils/ProtectedRoute";

import { Route, Routes, useLocation } from "react-router-dom";
import AllCard from "./components/task/AllCard";
import AddNewTask from "./components/task/AddNewTask";
import AddNewCategory from "./components/category/AddNewCategory";
import Root from "./route/Rout";

// import ReviewStatus from './components/category/status/ReviewStatus';
import DoneStatus from "./components/category/status/DoneStatus";
import InProgress from "./components/category/status/InProgress";
import NotYetStatus from "./components/category/status/NotYetStatus";
import UpdateTask from "./components/task/UpdateTask";
import ReviewStatus from "./components/category/status/ReviewStatus";
import Category from "./components/category/Category";
import Categories from "./components/category/Categories";
import NotFound from "./components/register_login/NotFound";

import AllCategories from "./components/category/Categories";
import Rout from "./route/Rout";
import { useState } from "react";
function App() {
//   const location = useLocation();
//   console.log(location);
//   const  token = localStorage.getItem('token')
// console.log(token);

const [isStatus,setStatus] = useState("")

console.log("this is status",isStatus)
  return (
    <div>
    
      <Routes>
        <Route index path="/" element={<LandingPage />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/register" element={<Register />}></Route>

      
        <Route path="/home/" element={<PrivateRoutes children={<Root />} />}>
          <Route path="/home/" element={<Root />}>
            <Route path="dashboard" element={<AllCard />} />
            <Route path="addNewTask" element={<AddNewTask />} />
            <Route path="addNewCategory" element={<Categories />} />
            <Route path="DoneStatus" element={<DoneStatus />} />
            <Route path="InProgress" element={<InProgress />} />
            <Route path="NotYetStatus" element={<NotYetStatus />} />
            <Route path="UpdateTask" element={<UpdateTask />} />
            <Route path="ReviewStatus" element={<ReviewStatus />} />
          </Route>
        </Route>

          <Route path="*" element={<NotFound/>}/>
      </Routes>
    </div>
  );
}

export default App;
