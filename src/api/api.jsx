import axios from "axios";

const  token = localStorage.getItem('token');
// const token = JSON.parse(localStorage.getItem("token"));
// const  token = JSON.parse(localStorage.getItem('token'))

export const api = axios.create({
  baseURL: "http://localhost:8080/api/v1",
  headers: {
   
      // " Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ2aWRpamFtZUBnbWFpbC5jb20iLCJpYXQiOjE2ODA4NTY5NjQsImV4cCI6MTY4MDg3MTM2NH0.v00ub-umIJD9UPVlUeVU-8Ftcn_dlfm4EaN5mGZJL2g",

      // " Bearer " `${token}`,
     
    Authorization: `Bearer ${token}`, 
    "Content-Type": "application/json",
  },
});
