import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { getAllTaskCurrentUser } from "../app/service/taskService";
import { useDispatch } from "react-redux";

const HomePage = () => {
  const [isOpen, setOpen] = useState(false);
  const handleDropDown = () => {
    setOpen(!isOpen);
  };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllTaskCurrentUser());
  }, [dispatch]);
  const navigate = useNavigate();
  const handleLogOut = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <div>
      <button
        data-drawer-target="sidebar-multi-level-sidebar"
        data-drawer-toggle="sidebar-multi-level-sidebar"
        aria-controls="sidebar-multi-level-sidebar"
        type="button"
        class="inline-flex items-center p-2 mt-2 ml-3 text-slate-50  text-sm text-gray-500 rounded-lg md:hidden hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-gray-200"
      >
        {/* <span className="sr-only">Open sidebar</span> */}
        <svg
          className="w-6 h-6"
          aria-hidden="true"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            clip-rule="evenodd"
            fill-rule="evenodd"
            d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z"
          ></path>
        </svg>
      </button>
      <aside
        id="sidebar-multi-level-sidebar"
        className="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0"
        aria-label="Sidebar"
      >
        <div className="h-full px-3 py-4 overflow-y-auto bg-sky-900 text-slate-50">
          <a href="#" className="flex items-center pl-2.5 my-9">
            <label className="w-6 h-6 mr-3.5 rounded-full bg-white text-sky-900 text-center">
              Z
            </label>
            <span className="self-center text-2xl font-semibold whitespace-nowrap font-Signika">
              Workspace
            </span>
          </a>

          <ul className="space-y-2 font-medium">
            <li>
              <NavLink
                to="/home/dashBoard"
                className="flex items-center p-2 text-gray-900 rounded-lg hover:bg-sky-700"
              >
                <svg
                  aria-hidden="true"
                  className="w-6 h-6 text-white transition duration-75"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                  <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
                </svg>
                <span className="ml-3 text-slate-50  ">Dashboard</span>
              </NavLink>

              <NavLink
                to="/home/addNewCategory"
                className="flex items-center p-2 text-gray-900 rounded-lg hover:bg-sky-700"
              >
                <svg
                  aria-hidden="true"
                  className="w-6 h-6 text-white transition duration-75"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
                <span className="ml-3 text-slate-50 ">Category</span>
              </NavLink>
            </li>
            <li>
              <button
                onClick={handleDropDown}
                type="button"
                className="flex items-center w-full p-2 text-gray-900 transition duration-75 rounded-lg group hover:bg-sky-700"
                aria-controls="dropdown-example"
                data-collapse-toggle="dropdown-example"
              >
                <span
                  className="flex-1 ml-3 text-slate-50   text-left whitespace-nowrap"
                  sidebar-toggle-item
                >
                  Status
                </span>
                <svg
                  className="w-6 h-6 ml-6"
                  aria-hidden="true"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d={`${
                      isOpen
                        ? "M14.707 12.707a1 1 0 01-1.414 0L10 9.414l-3.293 3.293a1 1 0 01-1.414-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 010 1.414z"
                        : "M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    }`}
                    clip-rule="evenodd"
                  ></path>
                </svg>
              </button>
              <ul
                id="dropdown-example"
                className={`py-2 space-y-2 ${isOpen ? "block" : "hidden"}`}
              >
                <li>
                  <NavLink
                    to="/home/DoneStatus"
                    className="flex items-center w-full p-2 text-slate-50 transition duration-75 rounded-lg pl-11 group hover:bg-sky-700 active"
                  >
                    <svg
                      aria-hidden="true"
                      className="w-6 h-6 text-white transition duration-75"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path>
                      <path
                        fill-rule="evenodd"
                        d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm9.707 5.707a1 1 0 00-1.414-1.414L9 12.586l-1.293-1.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                    Done
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/home/InProgress"
                    className="flex items-center w-full p-2 text-slate-50  transition duration-75 rounded-lg pl-11 group hover:bg-sky-700 active"
                  >
                    <svg
                      aria-hidden="true"
                      className="w-6 h-6 text-white transition duration-75"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                    Progress
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/home/ReviewStatus"
                    className="flex items-center w-full p-2 text-slate-50  transition duration-75 rounded-lg pl-11 group hover:bg-sky-700 active"
                  >
                    <svg
                      aria-hidden="true"
                      className="w-6 h-6 text-white transition duration-75"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                    Reviews
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/home/NotYetStatus"
                    className="flex items-center w-full p-2 text-slate-50  transition duration-75 rounded-lg pl-11 group hover:bg-sky-700 active"
                  >
                    <svg
                      className="w-6 h-6 text-white transition duration-75"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                      aria-hidden="true"
                    >
                      <path d="M5 4a2 2 0 012-2h6a2 2 0 012 2v14l-5-2.5L5 18V4z"></path>
                    </svg>
                    Not Yet
                  </NavLink>
                </li>
              </ul>
            </li>
            <li>
              <a
                href="/"
                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-sky-700 dark:hover:bg-gray-700"
              >
                <svg
                  className="w-5 h-5 mr-1 text-red-500"
                  aria-hidden="true"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M11 6a3 3 0 11-6 0 3 3 0 016 0zM14 17a6 6 0 00-12 0h12zM13 8a1 1 0 100 2h4a1 1 0 100-2h-4z"></path>
                </svg>
                <span
                  className="flex-1 ml-3 text-slate-50  whitespace-nowrap text-red-500"
                  onClick={handleLogOut}
                >
                  Log Out
                </span>
              </a>
              {/* <button type="submit" className="bg-red-500" onClick={handleLogOut}>Logout</button> */}
            </li>
          </ul>
        </div>
      </aside>
      {/* <AllCard/> */}
      {/* <AddNewTask/> */}
      {/* <AddNewCategory/> */}
    </div>
  );
};

export default HomePage;
