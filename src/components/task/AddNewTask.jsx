import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNewTask } from "../../app/service/taskService";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { getAllCategory } from "../../app/service/categoryService";
// import { log } from "console";

const AddNewTask = () => {
  const [taskName, setTaskName] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  const [categoryId, setCategory] = useState(2);
  const [status, setStatus] = useState("");

  useEffect(() => {
    dispatch(getAllCategory());
  },[]);

  const allCategories = useSelector((state)=> state.categoryData.category);
  console.log("allCategory : ",allCategories);

  const dispatch = useDispatch();
  const link = useNavigate();
  const addHandler = (event) => {
    event.preventDefault();
    const dateConvert = new Date();
    let newDate = dateConvert.toISOString(date);
    const param = {
      taskName,
      description,
      newDate,
      status,
      categoryId,
    };
    dispatch(addNewTask(param));
    link("/home/dashboard");
  };

  const back = (event) => {
    link("/home/dashboard");
  };

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  const handleCategoryStateChange = (e) => {
      setCategory(e.target.value);     
  }

  return (
    <div className="p-4 sm:ml-80  mt-14 mr-48">
      <nav className="bg-white border-gray-200 dark:bg-gray-900">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between pb-8">
          <h1 className="text-5xl font-black">Add New Task</h1>
        </div>
      </nav>
      <form>
        <div class="justify-between flex">
          <div className="">
            <label
              onChange={(e) => setDate(e.target.value)}
              for="date"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Date
            </label>
            <input
              type="datetime-local"
              class="shadow-md bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-60 p-2.5"
              required
            />
          </div>
          <div className="mb-3 mr-10">
          {/* <div className=""> */}
            <label
              for="category"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Category
            </label>
            <select
              // onChange={(e) => setCategory(e.target.value)}
              id="small"
              class="shadow-md block w-60 p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
              onChange={handleCategoryStateChange}
            >
              {allCategories.map((e) => (
                <option  value={e.categoryId}>{e.categoryName}</option>
              ))}
              
            </select>
          </div>
          <div className="">
            <label
              for="status"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white "
            >
              Status
            </label>
            <select
              onChange={(e) => setStatus(e.target.value)}
              class="shadow-md block w-60 p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
            >
              <option selected disabled>
                Choose Status
              </option>
              <option value="Done">Done</option>
              <option value="Review">Review</option>
              <option value="In_Progress">In_Progress</option>
              <option value="Not_Yet">Not_Yet</option>
            </select>
          </div>
          <div className="-mb-24">
            <svg
              class="w-60 h-60 text-gray-400 dark:text-gray-500 group-hover:text-blue-600 dark:group-hover:text-blue-500"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path>
              <path
                fill-rule="evenodd"
                d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </div>
        </div>
        <div class="-mt-10">
          <label
            for="Title"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Title
          </label>
          <input
            onChange={(e) => setTaskName(e.target.value)}
            type="text"
            class="w-9/12 shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
            required
          />
        </div>
        <div class="mt-10">
          <label
            for="message"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Description
          </label>
          <textarea
            onChange={(e) => setDescription(e.target.value)}
            id="message"
            rows="4"
            class="w-full block p-2.5 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
            placeholder="Leave a comment..."
          ></textarea>
        </div>
        <div className="mt-6">
          <NavLink to={"/home/dashboard"}>
            <button
              type="submit"
              class="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2"
            >
              Cancel
            </button>
          </NavLink>

          <Link>
            <button
              onClick={addHandler}
              type="submit"
              class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Create
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default AddNewTask;
