import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
// import { getAllTaskCurrentUser } from "../../app/service/taskService";
import moment from "moment";
import Swal from "sweetalert2";
import { deleteTask } from "../../app/service/taskService";

const AllCard = () => {
  const [data, setData] = useState([]);

  const dispatch = useDispatch();

  console.log("Data : ", data);
  const allTask = useSelector((state) => state.taskData.task);
  // console.log("task data ", allTask);

  const getKey = (id) => {
    // console.log(id);
  };

  const getData = (event) => {
    setData([event]);
  };

  const navigate = useNavigate();
  const directUpdate = (e) => {
    navigate("/home/UpdateTask", { state: { task: e } });
    console.log("e data", e);
  };
  // const deleteTask = (id) => {

  //   // console.log("delete", id);
  // };
  const sweetAlert = (id) => {
    // console.log("delete id : ", id);

    Swal.fire({
      text: "Are you sure you want to delete this task!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes I am sure",
      width: "400px",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          width: "200px",
        });
        dispatch(deleteTask(id));
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  return (
    <div className="p-4 sm:ml-80  mt-14">
      <nav className="bg-white border-gray-200 dark:bg-gray-900">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between pb-14">
          <h1 className="text-5xl font-black">All your boards</h1>
          <div className="flex md:order-2">
            <NavLink
              to="/home/addNewTask"
              className="px-5 py-2.5 text-center mr-2 mb-2 text-base bg-sky-900 hover:bg-sky-700 text-slate-50 rounded-3xl"
            >
              Add new task
            </NavLink>
          </div>
        </div>
      </nav>
      <>
        <input type="checkbox" id="my-modal-3" class="modal-toggle" />
        <div class="modal">
          <div class="modal-box relative">
            {data?.map((e) => (
              <div>
                <label
                  for="my-modal-3"
                  class="btn btn-sm btn-circle absolute right-2 top-2"
                >
                  ✕
                </label>
                <div class="text-lg font-bold mt-5 flex justify-between ">
                  <h4>{e.taskName}</h4>
                  <h4>
                    {" "}
                    <b className="text-red-800">
                      {moment(e?.date).format("ddd, MMM D")}
                    </b>
                  </h4>
                </div>
                <hr />
                <div class="text-md font-normal mt-5 flex justify-between">
                  <h4>{e.description}</h4>
                  <h4>
                    {" "}
                    <b className="text-cyan-400">{e.status}</b>
                  </h4>
                </div>
                <div className="mt-5 flex justify-end">
                  <button
                    onClick={() => directUpdate(e)}
                    class="btn btn-active btn-accent rounded-md mr-2 "
                  >
                    Update
                  </button>
                  {/* <button class="btn btn-outline rounded-md" for="my-modal-3">
                    Cancel
                  </button> */}
                  <label for="my-modal-3" class="btn btn-outline rounded-md">
                    Cancel
                  </label>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
      <div className="grid grid-cols-2 md:grid-cols-3">
        {allTask?.map((event, index) => (
          <button
            data-modal-target="defaultModal"
            data-modal-toggle="defaultModal"
          >
            <div key={index} onClick={() => getKey(event.taskId)}>
              <div
                className={`${
                  event.status === "Done"
                    ? "text-white bg-[#097488]"
                    : event.status === "Review"
                    ? "text-white bg-[#AD6204] "
                    : event.status === "In_Progress"
                    ? "text-white bg-[#498964]"
                    : event.status === "Not_Yet"
                    ? "text-white bg-[#a43627]"
                    : "bg-slate-200"
                } p-6 border-gray-200 rounded-md shadow w-10/12 h-10/12 mt-10 `}
              >
                <button
                  onClick={() => sweetAlert(event.taskId)}
                  className="float-right hover:bg-slate-300 rounded-full bg-slate-300 text-2xl font-bold"
                >
                  <svg
                    aria-hidden="true"
                    class="w-7 h-7"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </button>
                <label for="my-modal-3" onClick={() => getData(event)}>
                  <h5 className="mb-2 text-2xl font-bold tracking-tight dark:text-white text-justify">
                    {moment(event?.date).format("ddd, MMM D")}
                  </h5>
                  <p className="mb-2 text-2xl font-bold tracking-tight dark:text-white line-clamp-1 text-justify">
                    {event.taskName}
                  </p>

                  <p className="mb-3 font-normal line-clamp-3 text-justify">
                    {event.description}
                  </p>
                </label>
                <button className="mb-3 font-normal bg-white text-black rounded-md p-2  line-clamp-3 text-justify">
                  {event.status}
                </button>
              </div>
            </div>
          </button>
        ))}
      </div>
    </div>
  );
};

export default AllCard;
