import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, useLocation, useNavigate } from "react-router-dom";
import { updateTask } from "../../app/service/taskService";

const UpdateTask = () => {
  const location = useLocation();
  // console.log("get value from allcard", location.state.task);
  const { task } = location.state;
  const allTask = useSelector((state) => state.taskData.task);
  console.log("task data ", allTask);
  const [taskName, setTaskName] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  const [categoryId, setCategory] = useState(2);
  const [status, setStatus] = useState("");
  const [taskId, setTaskId] = useState(task.taskId);

  console.log("TaskID : ", taskId);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const OnUpdate = (event) => {
    event.preventDefault();
    console.log(taskName);
    console.log(description);
    console.log(date);
    console.log(categoryId);
    console.log(status);

    const dateConvert = new Date();
    let newDate = dateConvert.toISOString(date);

    const param = {
      taskId,
      taskName,
      description,
      newDate,
      status,
      categoryId,
    };

    dispatch(updateTask(param));
    navigate("/AllCard");
  };

  return (
    <div className="p-4 sm:ml-80  mt-14 mr-48">
      <nav className="bg-white border-gray-200 dark:bg-gray-900">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between pb-8">
          <h1 className="text-5xl font-black">Add New Task</h1>
        </div>
      </nav>
      <form onSubmit={OnUpdate}>
      <div class="justify-between flex">
          <div className="">
            <label
              for="date"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              onChange={(e) => setDate(e.target.value)}
            >
              Date
            </label>
            <input
              onChange={(e) => setDate(e.target.value)}
              type="datetime-local"
              class="shadow-md bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-60 p-2.5"
              required
            />
          </div>
          <div className="mb-3 mr-10">
            <label
              for="category"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Category
            </label>
            <select
              onChange={(e) => setCategory(e.target.value)}
              id="small"
              class="shadow-md block w-60 p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
            >
              <option selected disabled>
                Choose Category
              </option>
              <option value=""></option>
              <option value=""></option>
              <option value=""></option>
            </select>
          </div>
          <div className="mb-3 mr-10">
            <label
              for="status"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Status
            </label>
            <select
              onChange={(e) => setStatus(e.target.value)}
              class="shadow-md block w-60 p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
            >
              <option selected disabled>
                Choose Status
              </option>
              <option value="Done">Done</option>
              <option value="Review">Review</option>
              <option value="In_Progress">In_Progress</option>
              <option value="Not_Yet">Not_Yet</option>
            </select>
          </div>
          <div className="-mb-24">
            <svg
              class="w-60 h-60 text-gray-400 dark:text-gray-500 group-hover:text-blue-600 dark:group-hover:text-blue-500"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path>
              <path
                fill-rule="evenodd"
                d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </div>
        </div>
        <div class="-mt-10">
          <label
            for="Title"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Title
          </label>
          <input
            onChange={(e) => setTaskName(e.target.value)}
            type="text"
            placeholder={task.taskName}
            class="w-9/12 shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
            required
          />
        </div>
        <div class="mt-10">
          <label
            for="message"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Description
          </label>
          <textarea
            onChange={(e) => setDescription(e.target.value)}
            id="message"
            rows="4"
            class="w-full block p-2.5 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
            placeholder={task.description}
          ></textarea>
        </div>
        <div className="mt-6">
          <NavLink to={"/home/dashboard"}>
            <button
              type="button"
              class="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2"
            >
              Cancel
            </button>
          </NavLink>

          <button
            type="submit"
            class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Update
          </button>
        </div>
      </form>
    </div>
  );
};

export default UpdateTask;
