import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { userLogin } from '../../app/service/authService';


const Login = () => {
  const dispatch = useDispatch()
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");
  const [status, setStatus] = useState();
  const [open, setOpen] = useState(false);
  const navigate = useNavigate()
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },

    validationSchema: Yup.object({
      email: Yup.string()
        .email('Invalid email address')
        .required('Please Input Email'),
      password: Yup.string()
        .required('Please Input Password'),
    }),

    onSubmit: values => {
      const { email, password } = values
      dispatch(userLogin({ email, password }))
        .then((res) => {
          // console.log("res ", res.payload);
          if (res.payload == 404) {
            setStatus(res.payload)
            setOpen(!open)
          } else if (res.payload == 403) {
            setStatus(res.payload)
            setOpen(!open)
          } else {
            navigate("/home")
          }
        })
    },

  });
  // console.log("Status : ",status);

  return (
    <div>
      <section className="h-screen">
        <div className="container h-full px-6 py-12">
          <div className="g-6 flex h-full flex-wrap items-center justify-center lg:justify-between">
            <div className="md:w-8/12 lg:ml-6 lg:w-5/12">
              <p className="mb-10 text-2xl">User Login ! </p>
              <form onSubmit={formik.handleSubmit}>
                <div className="mb-6">
                  <label
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Your email
                  </label>
                  <input
                    type="email"
                    id="email"
                    name="email"
                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                    placeholder="name@gmail.com"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.email}
                  />
                   {formik.touched.email && formik.errors.email && (
                    <span className='text-red-600'>{formik.errors.email}</span>
                  )}
                </div>

                <div className="mb-6">
                  <label
                    className="block mb-2 text-sm font-medium text-gray-900"
                  >
                    Your password
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 "
                    placeholder="Password"
                    onChange={formik.handleChange}
                    value={formik.values.password}
                    onBlur={formik.handleBlur}
                  />
                   {formik.touched.password && formik.errors.password && (
                    <span className='text-red-600'>{formik.errors.password}</span>
                  )}
                </div>
                <button
                  type="submit"
                  className="inline-block bg-blue-500 w-full rounded bg-primary px-7 pt-3 pb-2.5 text-sm font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                  Login
                </button>
              </form>
        
              {status == 404 && open &&
                <div>
                  <div  className="fixed top-0 left-0 right-0 z-50  w-50  p-4 overflow-x-hidden overflow-y-auto ">
                    <div className=" w-full h-full max-w-2xl">
                      <div className="relative bg-white rounded-lg shadow">
                        <div className="flex items-start justify-between p-4 border-b rounded-t">
                          <h3 className="text-xl font-semibold text-red-600  ">
                            Wrong Email Or Password
                          </h3>
                          <button
                            onClick={() => setOpen(false)}
                            type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" data-modal-hide="defaultModal">
                            <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" ></path></svg>
                            <span className="sr-only">Close modal</span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              }
              {status == 403 && open &&
                <div>
                  <div  className="fixed top-0 left-0 right-0 z-50  w-50  p-4 overflow-x-hidden overflow-y-auto ">
                    <div className=" w-full h-full max-w-2xl">
                      <div className="relative bg-white rounded-lg shadow">
                        <div className="flex items-start justify-between p-4 border-b rounded-t">
                          <h3 className="text-xl font-semibold text-red-600 ">
                            Wrong Password
                          </h3>
                          <button
                            onClick={() => setOpen(false)}
                            type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" data-modal-hide="defaultModal">
                            <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" ></path></svg>
                            <span className="sr-only">Close modal</span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              }


            </div>
            <div className="mb-12 md:mb-0 md:w-8/12 lg:w-6/12">
              <img
                // src="https://tecdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                src="https://i.pinimg.com/736x/6b/1b/22/6b1b22573f9f3d4bba11a9fa5cb45652.jpg"
                className="w-full"
                alt="Phone image"
              />
            </div>
          </div>
        </div>
      </section >
    </div >
  )
}

export default Login