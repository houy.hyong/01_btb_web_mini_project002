import React from 'react'

const NotFound = () => {

  return (

    <div className="grid grid-cols-2 bg-white h-screen">
      <div>
        <img src="https://img.freepik.com/premium-vector/error-404-illustration-concept-white-background_701961-3097.jpg?w=2000" alt="" srcset="" />
      </div>
      <div className="text-2xl text-center mt-40 font-bold text-red-600">
        Sorry , This Page Is Not Found !!
      </div>

    </div>
  )
}

export default NotFound