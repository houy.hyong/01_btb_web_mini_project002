// import React from 'react'
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getDoneStatus } from "../../../app/service/taskService";
import moment from "moment";
const DoneStatus = () => {
  const done = useSelector((state) => state.taskData.done);

  console.log("doneStatus data", done);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDoneStatus());
  }, [dispatch]);
  return (
    <div className="p-4 sm:ml-80  mt-14">
      <nav className="bg-white border-gray-200 dark:bg-gray-900">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between pb-14">
          <h1 className="text-5xl font-black">All your boards</h1>
          <div className="flex md:order-2">
            <NavLink
              to="/home/addNewTask"
              className="px-5 py-2.5 text-center mr-2 mb-2 text-base bg-sky-900 hover:bg-sky-700 text-slate-50 rounded-3xl"
            >
              Add new task
            </NavLink>
          </div>
        </div>
      </nav>
      <div className="grid grid-cols-2 md:grid-cols-3 gap-4">
        {done?.map((event, index) => (
          <div key={index}>
            <div
              className={`${
                event.status === "Done"
                  ? "text-white bg-[#097488]"
                  : event.status === "Review"
                  ? "text-white bg-[#AD6204] "
                  : event.status === "In_Progress"
                  ? "text-white bg-[#498964]"
                  : event.status === "Not_Yet"
                  ? "text-white bg-[#a43627]"
                  : "bg-slate-200"
              } max-w-sm p-6 border-gray-200 rounded-lg shadow`}
            >
              <button className="float-right hover:bg-slate-300 rounded-full bg-slate-300 text-2xl font-bold">
                <svg
                  aria-hidden="true"
                  class="w-7 h-7"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
              </button>
              <a href="#">
                <h5 className="mb-2 text-2xl font-bold tracking-tight ">
                  {moment(event?.date).format("ddd, MMM D")}
                </h5>
                <p className="mb-2 text-2xl font-bold tracking-tight line-clamp-1">
                  {event.taskName}
                </p>
              </a>
              <p className="mb-3 font-normal line-clamp-3 text-justify">
                {event.description}
              </p>
              <button className="mb-3 font-normal bg-white rounded-md p-2 text-black line-clamp-3 text-justify">
                {event.status}
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DoneStatus;
