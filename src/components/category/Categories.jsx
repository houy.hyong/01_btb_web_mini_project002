import CreateCategoryCard from "./CreateCategoryCard";
import ShowCategory from "./ShowCategory";
import AddNewCategory from "./AddNewCategory";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addNewCategory,
  deleteCategory,
  getAllCategory,
} from "../../app/service/categoryService";
import moment from "moment";
import UpdateCategory from "./UpdateCategory";
import DeleteCategory from "./DeleteCategory";
import { Button, Modal } from "flowbite-react";
import Swal from "sweetalert2";

const Categories = () => {
  const [updateId, setUpdateId] = useState();
  const [deleteId, setDeleteId] = useState();

  const category = useSelector((state) => state.categoryData.category);
  
  // console.log("category data", category);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllCategory());
    console.log(category);
  },[dispatch]);

  const getId = (id) => {
    setUpdateId(id);
    setDeleteId(id);
  };

  // const [isOpen, setOpen] = useState(false);
  // const openModal = () => {
  //   setOpen = true;
  // };
  // const closeModal = () => {
  //   setOpen = false;
  // };
  const deleteCategoryHandler = (deleteId) => {
    // e.preventDefault();
    Swal.fire({
      text: "Are you sure you want to delete this category!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes I am sure",
      width: "300px",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          width: "300px",
          icon: "success",
        });
        const body = {
          deleteId,
        };
        dispatch(deleteCategory(body));
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  return (
    <div className="p-4 sm:ml-80  mt-14">
      <nav className="bg-white border-gray-200 dark:bg-gray-900">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between pb-16">
          <h1 className="text-5xl font-black">Categories</h1>
        </div>
      </nav>

      <div className="grid grid-cols-2 md:grid-cols-3 gap-4">
        <CreateCategoryCard />

        {category?.map((event, index) => (
          <div
            key={index}
            className="rounded-lg shadow w-11/12 h-28 mb-2 select-none border-l-4 p-4 font-medium hover:border-cyan-600 border-sky-900 bg-sky-50"
          >
            <a href="#">
              <div>
                <div className="flex justify-between items-center">
                  <label className="font-black text-2xl"> {event.categoryName} </label>

                  <div class="dropdown dropdown-bottom">
                    <label
                      tabindex="0"
                      className="bg-sky-50 hover:bg-sky-50 hover:border-sky-50 border-sky-50 btn m-0 text-2xl text-sky-900 inline-block align-text-top"
                    >
                      ...
                    </label>
                    <ul
                      tabindex="0"
                      className="dropdown-content menu p-4 shadow bg-base-100  rounded-box w-52 mb-0 space-y-3.5 px-6 space-x-0.5 mx-5"
                    >
                      <li onClick={() => getId(event.categoryId)}>
                        <label
                          for="my-modal"
                          class="btn btn-outline btn-accent"
                          // onClick={()=>console.log("button id",event.categoryId)}
                          onClick={() => <UpdateCategory />}
                        >
                          Edit
                        </label>
                      </li>

                      <li
                        onClick={() => {
                          getId(event.categoryId);
                        }}
                      >
                        {/* <button class="btn btn-outline btn-error">Delete</button> */}
                        <label
                          for="my-modal1"
                          class="btn btn-outline btn-error"
                          // onClick={() => {
                          //   console.log("button id", event.categoryId);
                          // }}
                          onClick={() =>
                            deleteCategoryHandler(event.categoryId)
                          }

                          // onClick={()=>(setId(index), console.log("button id",setId))}
                        >
                          Delete
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <p>Created : {moment(event?.date).format("ddd, MMM D")}</p>
            </a>
            <UpdateCategory updateId={updateId} />
            {/* <DeleteCategory deleteId={deleteId}/> */}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Categories;
