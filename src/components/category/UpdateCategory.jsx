import React, { useState } from 'react'
import { updateCategory } from '../../app/service/categoryService';
import { useDispatch } from 'react-redux';

const UpdateCategory = ({updateId}) => {

    const dispatch = useDispatch();

    const [categoryName, setCategoryName] = useState("");

    const updateCategoryHandler = (e) => {
        e.preventDefault();
        const body = {
            categoryName,
            updateId
        }
        dispatch(updateCategory(body));
    };
    console.log("id",updateId)

  return (
    <div>
      <input type="checkbox" id="my-modal" class="modal-toggle" />
      <div class="modal">
        <div class="modal-box relative">
          <label
            for="my-modal"
            class="btn btn-sm btn-circle absolute right-2 top-2 bg-blue-600 hover:bg-blue-600"
          >
            ✕
          </label>
          <h3 class="text-2xl font-bold mt-2 mb-6">Update Category</h3>
          <hr className="bg-gray-400 p-0 mb-6"></hr>

          <form onSubmit={updateCategoryHandler}>
            <div class="mb-6">
              <label 
                class="block mb-2.5 text-base font-normal text-gray-900 dark:text-white pl-1.5"
              >
                Category Name
              </label>
              <input
                type="text"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="update category"
                onChange={(e) => setCategoryName(e.target.value)}
                required
              />
            </div>

            <div className="mb-6">
                <button
                  type="submit"
                  className="text-white bg-sky-900 hover:bg-sky-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
                >
                  <label htmlFor="my-modal">Update</label>
                </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default UpdateCategory
