import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addNewCategory,
  getAllCategory,
} from "../../app/service/categoryService";
import moment from "moment";
import ShowCategory from "./ShowCategory";
import { Link, useNavigate } from "react-router-dom";

const AddNewCategory = () => {
  // const category = useSelector((state) => state.categoryData.category);
  // // const [open , setOpen] = useState(false)
  // console.log("category data", category);

  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(getAllCategory());
  // }, [dispatch]);

  const [categoryName, setCategory] = useState("");
  const [open, setOpen] = useState(false);

  const addCategoryHandler = (e) => {
    e.preventDefault();
    e.target.reset();
    const body = {
      categoryName,
    };
    dispatch(addNewCategory(body));
    e.target.reset();
  };

  return (
    <div>
      <input type="checkbox" id="my-modal-3" class="modal-toggle" />
      <div class="modal">
        <div class="modal-box relative">
          <label
            for="my-modal-3"
            class="btn btn-sm btn-circle absolute right-2 top-2 bg-blue-600 hover:bg-blue-600"
          >
            ✕
          </label>
          <h3 class="text-2xl font-bold mt-2 mb-6">Create Category</h3>
          <hr className="bg-gray-400 p-0 mb-6"></hr>


          <form onSubmit={addCategoryHandler}>
            <div class="mb-6">
              <label class="block mb-2.5 text-base font-normal text-gray-900 dark:text-white pl-1.5">
                Category Name
              </label>
              <input
                type="text"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Create Category"
                onChange={(e) => setCategory(e.target.value)}
                required
              />
            </div>

            <div className="mb-6">
              {/* <button
                type="submit"
                class="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                onClick={addCategoryHandler}
              >
                Create
              </button> */}
                <button
               
               
                  type="submit"
                  className="text-white bg-sky-900 hover:bg-sky-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
                >
                  <label htmlFor="my-modal-3">Submit</label>
                </button>
                {/* <button onClick={() => setOpen(true)}
               
               
                  type="submit"
                  className="text-white bg-sky-900 hover:bg-sky-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
                >
                  <label htmlFor="my-modal-3" >Submit</label>
                </button> */}
          
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AddNewCategory;
