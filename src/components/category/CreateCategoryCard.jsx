import React from 'react'
import AddNewCategory from './AddNewCategory'

const CreateCategoryCard = () => {
  return (
    <div>
        {/* --- */}
        <div className="rounded-lg shadow w-11/12 h-28 mb-2 text-gray-700 bg-gray-100 rounded-lg shadow bg-sky-50">
          <div className="flex justify-center">
            <div className="ml-3 text-sm font-normal">
              <p className="mb-5 text-lg font-bold ">Create Category</p>
              <div className="ml-10">
                {/* Action click popup */}
                <label
                  for="my-modal-3"
                  class="btn rounded-full w-12 h-10 bg-blue-600 hover:bg-blue-600"
                >
                  <svg
                    className="w-5 h-5 text-white font-bold tex-2xl"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                  >
                    <path
                      clip-rule="evenodd"
                      fill-rule="evenodd"
                      d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                    ></path>
                  </svg>
                </label>
              </div>
            </div>
          </div>
        </div>
        <AddNewCategory />
        {/* --- */}
    </div>
  )
}

export default CreateCategoryCard
