import React from 'react'
import HomePage from '../page/HomePage'
import { Outlet } from 'react-router-dom'

const Rout =() =>{
  return (
    <>
        <HomePage/>
        <Outlet/>
    </>
  )
}

export default Rout