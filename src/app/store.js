import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./slice/authSlice";
import categorySlice from "./slice/categorySlice";
import taskSlice from "./slice/taskSlice";


const store = configureStore({
    reducer :  {
        user : authSlice,
        categoryData : categorySlice,
        taskData: taskSlice,
    },
});

export default store;