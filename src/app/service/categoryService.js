import { createAsyncThunk } from "@reduxjs/toolkit";
import React from "react";
import { api } from "../../api/api";

//getAllCategory
export const getAllCategory = createAsyncThunk(
  "categories/getAllCategory",
  async () => {
    try {
      const response = await api.get(
        "/categories/users?page=1&size=20&asc=false&desc=true"
      );
      console.log("data", response.data.payload);
      return response.data.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

//addNewCategory
export const addNewCategory = createAsyncThunk(
  "addNewCategory",
  async (body) => {
    try {
      const response = await api.post("/categories/users", {
        categoryName: body.categoryName,
      });
      console.log("res", response.data);
      return response.data.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

//UpdateCategory
export const updateCategory = createAsyncThunk(
  "updateCategory",
  async (body) => {
    try {
      const response = await api.put(`/categories/${body.updateId}/users`, {
        categoryName: body.categoryName,
      });
      console.log("res", body);
      return response.data.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

//deleteCategory
export const deleteCategory = createAsyncThunk(
  "deleteCategory",
  async (body) => {
    console.log("service", body.deleteId);

    try {
      const response = await api.delete(`/categories/${body.deleteId}/users`);
      console.log("res", response.data);
      return body.deleteId;
    } catch (err) {
      console.log(err);
    }
  }
);
