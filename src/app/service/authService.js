import { createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios";
import { api } from "../../api/api";

export const userLogin = createAsyncThunk(
    'auth/login',
    async ({ email, password }) => {
      try {
        console.log(email,password);
        const res = await axios.post("http://localhost:8080/api/v1/auth/authentication", {
          email,
          password,
        },
        {
          headers:{ 'Content-Type': 'application/json' }
        }
        );
        localStorage.setItem('token',res.data.payload.token)
        
        return res
      } catch (error) {
          return error.response.status;
      }
    }
  )

  export const userRegister = createAsyncThunk(
    'auth/register',
    async ({email,password}) => {
      console.log(email,password);
      try{ 
          const response = await axios.post("http://localhost:8080/api/v1/auth/register",{
              email ,
              password 
          },
          )
          console.log(response);
      }catch(err){
          // console.log(err.response.status);
          return  err.response.status;
      }
  }
  )

const userService = {
    userLogin,
    userRegister
}

export default userService