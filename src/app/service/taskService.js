import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../../api/api";


// Get All Task Current User
export const getAllTaskCurrentUser = createAsyncThunk(
  "task/getAllTaskCurrentUser",
  async () => {
    try {
      
      const response = await api.get(
        "/tasks/users?page=1&size=10&asc=false&desc=false",
        {}
      );
      return response?.data?.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

// getDoneStatus
export const getDoneStatus = createAsyncThunk(
  "task/getDoneStatus",
  // async (status) => {
  async () => {
    try {
      // const response = await api.get("/tasks/status/users?status="+status);
      const response = await api.get("/tasks/status/users?status=Done");
      return response?.data?.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

// getReviewStatus
export const getReviewStatus = createAsyncThunk(
  "task/getReviewStatus",
  async () => {
    try {
      const response = await api.get("/tasks/status/users?status=Review");
      return response?.data?.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

// getInProgressStatus
export const getInProgressStatus = createAsyncThunk(
  "task/getInProgressStatus",
  async () => {
    try {
      const response = await api.get("/tasks/status/users?status=In_Progress");
      return response?.data?.payload;
    } catch (err) {
      console.log(err);
    }
  }
);
// getNotYetStatus
export const getNotYetStatus = createAsyncThunk(
  "task/getNotYetStatus",
  async () => {
    try {
      const response = await api.get("/tasks/status/users?status=Not_Yet");
      return response?.data?.payload;
    } catch (err) {
      console.log(err);
    }
  }
);

// addNewTask
export const addNewTask = createAsyncThunk("task/addNewTask", async (param) => {
  try {
    console.log(param);
    const response = await api.post("/tasks/users", {
      taskName: param.taskName,
      description: param.description,
      date: param.newDate,
      status: param.status,
      categoryId: param.categoryId,
    });
    console.log(response.data.payload);
    return response.data.payload;
  } catch (err) {
    console.log(err);
  }
});

// updateTaskById
export const updateTask = createAsyncThunk("task/updateTask", async (param) => {
  console.log("Data service ", param);
  try {
    const response = await api.put(`/tasks/${param.taskId}/users`, {
      taskName: param.taskName,
      description: param.description,
      date: param.newDate,
      status: param.status,
      categoryId: param.categoryId,
    });
    console.log(response);
    return response.data.payload;
  } catch (err) {
    console.log(err);
  }
});

export const deleteTask = createAsyncThunk("task/deleteTask", async (id)=>{
  console.log("id from delete : ", id);
  try{
    const response = await api.delete(`/tasks/${id}/users`)
    return id;
  }catch(err){
    console.log(err)
  }
})
