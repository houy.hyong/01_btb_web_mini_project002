import { createSlice } from '@reduxjs/toolkit'
import { userLogin, userRegister } from '../service/authService'


const initialState = {
    loading: false,
    userInfo: null,
    error: null,
    success: false,
  }

  const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: {
      // login user
      [userLogin.pending]: (state) => {
        state.loading = true
        state.error = null
      },
      [userLogin.fulfilled]: (state, action) => {
        state.loading = false
        state.userInfo = action.payload
      },
      [userLogin.rejected]: (state) => {
        state.loading = false
        state.error = "error"
      },
  
      [userRegister.pending]: (state) => {
          state.loading = true
          state.error = null
        },
        [userRegister.fulfilled]: (state, action) => {
          state.loading = false
          state.userInfo = action.payload
        },
        [userRegister.rejected]: (state) => {
          state.loading = false
          state.error = "error"
        },
  
  
      // register user reducer...
    },
  })
  export default authSlice.reducer