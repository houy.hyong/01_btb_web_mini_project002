import { createSlice } from "@reduxjs/toolkit";
import React from "react";
import {
  addNewCategory,
  deleteCategory,
  getAllCategory,
  updateCategory,
} from "../service/categoryService";

const initialState = {
  loading: false,
  error: "",
  category: [],
};

const categorySlice = createSlice({
  name: "category",
  initialState,
  reducers: {},

  extraReducers: {
    //getAllCategory
    [getAllCategory.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getAllCategory.fulfilled]: (state, action) => {
      state.loading = false;
      state.succuss = true;
      state.category = action.payload;
    },
    [getAllCategory.rejected]: (state) => {
      state.loading = false;
      state.error = [];
    },

    //addNewCategory
    [addNewCategory.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [addNewCategory.fulfilled]: (state, action) => {
      state.loading = false;
      state.succuss = true; //registration successful
      state.category.unshift(action.payload);
    },
    [addNewCategory.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    },

    //updateCategory
    [updateCategory.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [updateCategory.fulfilled]: (state, action) => {
      state.loading = false;

      const updateIndex = action.payload;
      console.log(updateIndex);

      const index = state.category.findIndex(
        (data) => data.categoryId == updateIndex.categoryId
      );
      console.log(index);

      if (index !== -1) {
        state.category[index] = updateIndex;
      }
    },
    [updateCategory.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    },

    //deleteCategory
    [deleteCategory.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [deleteCategory.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      const deleteIndex = action.payload;
      console.log("slice", deleteIndex);

      state.category = state.category.filter(
        (data) => data.categoryId !== deleteIndex
      );
      console.log("index", state.category);
    },
    [deleteCategory.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    },
  },
});

export default categorySlice.reducer;
