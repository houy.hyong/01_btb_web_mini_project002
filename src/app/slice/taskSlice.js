import { createSlice } from "@reduxjs/toolkit";
import {
  addNewTask,
  deleteTask,
  getAllTaskCurrentUser,
  getDoneStatus,
  getInProgressStatus,
  getNotYetStatus,
  getReviewStatus,
  updateTask,
} from "../service/taskService";

const initialState = {
  task: [],
  done: [],
  review: [],
  in_progress: [],
  not_yet: [],
  addTask:[],
  loading: false,
  error: "",
};
const taskSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    // getAllTaskCurrentUser
    [getAllTaskCurrentUser.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getAllTaskCurrentUser.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      state.task = action.payload;
    },
    [getAllTaskCurrentUser.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },

    // getDoneStatus
    [getDoneStatus.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getDoneStatus.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      state.done = action.payload;
    },
    [getDoneStatus.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },
    // getReviewStatus
    [getReviewStatus.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getReviewStatus.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      state.review = action.payload;
    },
    [getReviewStatus.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },
    // getInProgressStatus
    [getInProgressStatus.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getInProgressStatus.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      state.in_progress = action.payload;
    },
    [getInProgressStatus.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },
    // getNotYetStatus
    [getNotYetStatus.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [getNotYetStatus.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      state.not_yet = action.payload;
    },
    [getNotYetStatus.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },

    // addNewTask
    [addNewTask.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [addNewTask.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      state.task.unshift(action.payload);
      // state.addTask = action.payload;
    },
    [addNewTask.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },
    // updateTask
    [updateTask.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [updateTask.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      console.log("Payload Slice", action.payload);
      const UpdateData = action.payload;
      const index = state.task.findIndex(
        (data) => data.taskId === UpdateData.taskId
      );
      if (index !== -1) {
        state.task[index] = UpdateData;
      }
    },
    [updateTask.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },
    // deleteTask
    [deleteTask.pending]: (state) => {
      state.loading = true;
      state.error = null;
    },
    [deleteTask.fulfilled]: (state, action) => {
      state.loading = false;
      state.success = true;
      console.log("Payload Slice", action.payload);
      const DeleteData = action.payload;
      console.log("Hello Socheat", DeleteData);
      state.task = state.task.filter((data) => data.taskId !== DeleteData);
    },
    [deleteTask.rejected]: (state, action) => {
      state.loading = false;
      state.error = [];
    },
  },
});
export default taskSlice.reducer;
